---
title: Welcome
---

Welcome to **SteemCraft.com**, the world first public Minecraft server with STEEM based economy. Get paid for playing!

## Join us today!

![Join us - play.steemcraft.com](/images/welcome/ip.png)

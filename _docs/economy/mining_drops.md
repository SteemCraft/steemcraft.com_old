---
title: Mining money drops
category: Economy
order: 6
---

If you mine one of ores listed below (without [Silk Touch](https://minecraft.gamepedia.com/Enchanting#Silk_Touch) enchant), you have a little chance to get from 1 to 4mSBD:
* [Coal ore](https://minecraft.gamepedia.com/Coal_Ore)
* [Redstone ore](https://minecraft.gamepedia.com/Redstone_Ore)
* [Lapis Lazuli ore](https://minecraft.gamepedia.com/Lapis_Lazuli_Ore)
* [Emerald ore](https://minecraft.gamepedia.com/Emerald_Ore)
* [Diamond ore](https://minecraft.gamepedia.com/Diamond_Ore)

> If there is not enough money in [Loot wallet](/economy/loot_wallet/), you will get nothing.

Other ores (iron and gold) are not included because they drop ore (even if they are mined without [Silk Touch](https://minecraft.gamepedia.com/Enchanting#Silk_Touch))

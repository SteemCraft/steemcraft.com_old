---
title: Deposits and withdraws
category: Economy
order: 3
---

## Deposits

Each player can deposit own Steem Dollars on their server account using ``/deposit <amount in mSBD>`` command.

## Withdraws
> Due to limited account bandwidth, all withdrawals must be higher than 50mSBD.

Players can withdraw their mSBD from server account to ANY Steemit account using ``/withdraw <amount in mSBD> <account name>`` command.

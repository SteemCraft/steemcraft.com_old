---
title: Player balance
category: Economy
order: 1
---

> SteemCraft main currency is [Milli](https://en.wikipedia.org/wiki/Milli-) Steem Dollar (mSBD) - one thousandth Steem Dollar (0.001 SBD)

You can check your balance by typing ``/balance`` command.

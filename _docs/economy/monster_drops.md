---
title: Monster money drops
category: Economy
order: 4
---

> If there is not enough money in [Loot wallet](/economy/loot_wallet/), you will get nothing.

Each hostile monster (except of [Slimes](https://minecraft.gamepedia.com/Slime) and [Magma Cubes](https://minecraft.gamepedia.com/Magma_Cube)) can drop with little chance from 1 to **monster level** mSBD (chance increases if there is more mSBD in loot wallet).

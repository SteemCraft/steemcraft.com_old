---
title: Loot wallet
category: Economy
order: 2
---

"Loot wallet" is a public wallet from which players are given prizes for killing monsters, etc.

> You can deposit some SBD in loot wallet by making transaction to @steemcraft.com account with "loot" memo.

You can check "loot wallet" balance by using ``/lootwallet`` command.

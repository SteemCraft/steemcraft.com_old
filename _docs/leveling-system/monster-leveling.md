---
title: Monster leveling system
category: Leveling system
order: 2
---

The level of monsters depends mainly on the distance from the spawn and the type of map (The Overworld, Nether and End).

On each of them a different range of levels is set:

```
The Overworld: 1-20 level
The Nether: 20-40 level
The End: 40-60 level
```

From the level of the monster depends on his health, possible equipment and additional effects.

Monster health can be calculated from the formula: (with a few exceptions)

```
monster_level + 1
```

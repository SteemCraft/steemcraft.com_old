---
title: Player leveling system
category: Leveling system
order: 1
---

Each SteemCraft player starts with level zero. Every two levels players gains half heart.

You can calculate the amount of experience you need to the next level using the formula:

```
(next_level^2 * 256) - (current_level^2 * 256)
```

The result is that you need 256 experience points on the first level and 768 on the second level.


In the case of enchanting and repairing items, you do not lose your experience level, but you still need to own it (which means that you have a long way to level 30).

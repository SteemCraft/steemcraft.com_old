---
title: Special items
category: Players
order: 2
---

## Compass

Compass can teleport you to your bed location (home).

![Compass crafting](/images/special_items/compass.png)

## Clock

Clock can teleport you to server spawn.

![Clock crafting](/images/special_items/clock.png)

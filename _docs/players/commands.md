---
title: Commands
category: Players
order: 1
---

## Economy commands

``/send <player> <mSBD>`` sends \<mSBD\> to \<player\>

``/balance`` prints your balance

``/deposit <mSBD>`` generates SteemConnect transaction link

``/withdraw <mSBD> <account>`` withdraws \<mSBD\> from your server account to steem <account> account

## Chunk commands

``/claim`` claims chunk for you (cost: 25mSBD)

``/unclaim`` unclaims chunk.

``/access <nickname>`` allows/disallows \<nickname\> to place/break blocks, open chests etc. on your chunks

``/showchunk`` generates smoke practicles at chunk borders

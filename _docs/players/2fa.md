---
title: Two-factor authentication
category: Players
order: 900
---

> NOTE: If you lose your 2FA credentials, you will permanently lose access to your account on the server. We do not recomend using Google Authenticator app due to lack of "backup" option. Authy is a great alternative with cloud sync.

> If you don't know what "Two-factor authentication" is:
* [Multi-factor authentication - Wikipedia](https://en.wikipedia.org/wiki/Multi-factor_authentication)
* [Google Authenticator - Wikipedia](https://en.wikipedia.org/wiki/Google_Authenticator)
* [Time-based One-time Password Algorithm](https://en.wikipedia.org/wiki/Time-based_One-time_Password_Algorithm)

> **DO NOT ENABLE IT IF YOU DON'T KNOW WHAT ARE YOU DOING. YOU CAN LOST ACCESS TO STEEMCRAFT AND YOUR FUNDS**

## Step one: Get Authenticator app.

Authy is a great choice, simple Google Authenticator compatible app with cloud sync. Great for our use.

Get if from [Google Play (Android)](https://play.google.com/store/apps/details?id=com.authy.authy) or [App Store (iOS)](https://itunes.apple.com/us/app/authy/id494168017)

**Windows Phone/Mobile** users should use [Microsoft Authenticator](https://www.microsoft.com/pl-pl/store/p/microsoft-authenticator/9nblgggzmcj6) (Authy is available only for Android and iOS :/)

## Step two: Join server and enable 2FA

* Join the server.
* Type ``/2fa enable``
* Open authenticator app
* Scan QR code (you will get a map with QR code)
* Type ``/2fa <6 digit code from authenticator>``

## If you want to disable 2FA.

Just login to the server and type ``/2fa reset``

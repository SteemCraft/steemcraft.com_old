---
title: Connection problems
category: Players
order: 998
---

## "Invalid session"

Your Minecraft.net session is invalid (expired, you logged in on another computer, etc.). Try restarting game.


## "You are not white-listed on this server."

SteemCraft is probably in mainterance. Try again in few minutes.

## "Invalid host"

* you are connecting to wrong address (play.steemcraft.com)
* you have a problems with DNS (try connecting to **46.105.155.178:20000**)

## "[...] Connection refused."

SteemCraft is probably offline.

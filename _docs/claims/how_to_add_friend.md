---
title: How can I allow friend to build on my chunks?
category: Chunk claims
order: 3
---

If you want to allow your friend to build on your chunks. You can use ``/allow <nickname>`` command.

If you want to revoke his access to your chunks, type ``/allow <nickname>`` again.

---
title: How to claim chunk?
category: Chunk claims
order: 1
---

> Each player can claim up to 25 chunks. Each claim costs 25mSBD.

1. Stand on free chunk.
2. Type ``/claim`` [command](/players/commands/).

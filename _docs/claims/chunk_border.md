---
title: How can I see chunk borders?
category: Chunk claims
order: 4
---

## Method I - Minecraft built-in function

Use key combination ``F3+G`` to see chunk borders. Use it again to turn it off.

![Method 1](/images/chunks/show-1.png)

## Method II - /showchunk command

Use ``/showchunk`` [command](/players/commands/) to see chunk borders (smoke particles) for 5 seconds.

![Method 2](/images/chunks/show-2.png)

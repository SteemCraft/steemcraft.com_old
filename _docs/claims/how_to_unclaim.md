---
title: How to unclaim chunk?
category: Chunk claims
order: 2
---

1. Stand on one of your chunks.
2. Type ``/unclaim`` [command](/players/commands/).

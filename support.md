---
title: Support SteemCraft
permalink: /support
---

## If you are a content creator

You can use our busy.org refferal link: [https://busy.org/i/@steemcraft.com](https://busy.org/i/@steemcraft.com)

## If you are a developer

You know what you can, go there: [https://gitlab.com/SteemCraft](https://gitlab.com/SteemCraft)

## If you are a totally normal person with some savings on Steem

You can send some SBD to @steemcraft.com account (if you type "loot" in memo, all of yours money will go to the "loot wallet")
